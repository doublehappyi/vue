/**
 * Created by db on 16/1/28.
 */

var data = {
    toolbar:{
        keyword:"xx",
        isDelete:0
    },
    table:{
        list:[],
        page:1,
        totalNum:0,
        totalPage:0,
        pageSize:0
    },
    edit:{
        keyword:"",
        isDelete:""
    },
    add:{
        keyword:"",
        isDelete:""
    }
}

window.vdata = data;


var vm = new Vue({
    el:"#page",
    data: data,
    methods: {
        query:query,
        openEdit:openEdit,
        openAdd:openAdd,
        del:del
    },
    computed:{
        tableList:function(){
            for(var i = 0; i < data.table.list.length; i++){
                console.log(i, data.table.list[i]['isDelete']);
                data.table.list[i]['isDelete'] = data.table.list[i]['isDelete'] == 0 ? "否":"是";
            }
            return data.table.list;
        }
    }
});

query(data.toolbar.keyword, data.toolbar.isDelete, data.table.page);

function query(keyword, isDelete, page){
    var url = "./data/data.json?keyword=" + keyword + "&isDelete="+isDelete+"&page="+page;
    console.log(url);
    $.get(url).done(function(data){
        if(data.result){
            vm.$data.table = data.data;
        }
    });
}

function openEdit(keyword, isDelete){
    console.log(keyword, isDelete);
}

function del(keyword){
    console.log("del: ", keyword);
}

function openAdd(){
    console.log("openAdd");
}


